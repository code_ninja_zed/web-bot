var Nightmare = require('nightmare'),
  nightmare = Nightmare({show:true});

module.exports = function*(){
  var loggedIn;
  console.log('Attempting login...')
  nightmare.goto('https://accounts.craigslist.org/login/home')
    .wait(2000)
    .type('#inputEmailHandle', 'kidd1nine@aol.com')
    .type('#inputPassword', 'msh16ity!')
    .click('.login-box .accountform-btn')
    .wait(2000)
    .title()
    .end()
    yield nightmare.then(function(result){
      loggedIn = (result === 'craigslist account');
    })
    return loggedIn
}
