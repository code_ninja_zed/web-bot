var Nightmare = require('nightmare'),
  vo = require('vo'),
  nightmare = Nightmare({show:true});
require('nightmare-upload')(Nightmare);
var url = 'http://' + process.argv[2] + '.craigslist.org/';

module.exports = function * (ad, track) {
  console.log('Processing ad', track);
  yield nightmare.goto(url)
    .wait(2000)
    .click('#postlks #post')
    .click('input[value=ho]')
    .wait(2000)
    .click('.pickbutton')
    .wait(1000)
    .click('li:nth-child(8) input[type=radio]')
    .wait(1000)
    .type('#PostingTitle', ad.title)
    .type('#GeographicArea', ad.location)
    .type('#postal_code', ad.zip)
    .type('#PostingBody', ad.body)
    .insert('label input[name=Sqft]', ad.ft2)
    .insert('label input[name=Ask]', ad.rent)
    .select("label.std select[name=moveinMonth]", "option[value='12']")
    .type('label.std input[placeholder=day]', '')
    // .evaluate( function(cat) {
    //   document.querySelector('label.std input[placeholder=day]').value = 21
    // })
    .type('label.std input[placeholder=year]', '')
    // .evaluate( function(cat) {
    //   document.querySelector('label.std input[placeholder=year]').value = 1987
    // })
    .select('fieldset select#Bedrooms', 'option[value=3]')
    .select('fieldset select#bathrooms', 'option[value=7]')
    .select('fieldset select#private_room', '[value=1]')
    .select('fieldset select#private_bath', '[value=1]')
    .select('fieldset select#housing_type', 'option[value=1]')
    .select('fieldset select#laundry', 'option[value=1]')
    .select('fieldset select#parking', 'option[value=4]')
    .check('fieldset input#is_furnished')
    .type('input#xstreet0', ad.street)
    .type('input#city', ad.city)
    .click('#wantamap')
    .click('.bigbutton') // continue to image uplaoder
    .wait(1000)
    //.click('.bigbutton') // done w images
    .wait(1000)
    //.click('.bigbutton') // publish
    .wait(100000)
}
1827
20172017
/*
ft^2
 label input[name=Sqft]
 rent
 label input[name=Ask]

available on
  label.std select[name=moveinMonth] option[value='12']
  label.std input[placeholder=day]
  label.std input[placeholder=year]

bedrooms
  fieldset select#Bedrooms

bathrooms
  fieldset select#bathrooms

housing type
  fieldset select#housing_type

laundry
  fieldset select#laundry

parking
  fieldset select#parking

furnished
  fieldset input#is_furnished

street
  input#xstreet0

cross street
  input#xstreet1

city
  input#city

region
  input#region

continue
*/
